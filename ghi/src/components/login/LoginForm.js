import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handlePasswordChange,
  handleUsernameChange,
  error,
  reset,
} from "./loginSlice";
import { useLoginMutation } from "./auth";
import { useNavigate } from "react-router-dom";
import ErrorMessage from "../errorhandling/ErrorMessage";
import { Link } from "react-router-dom";
import "../../index.css"

const LoginForm = () => {
  const dispatch = useDispatch();
  const [login] = useLoginMutation();
  const { errorMessage, fields } = useSelector((state) => state.login);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(reset());

    try {
      const result = await login(fields).unwrap();
      if (result.access_token) {
        navigate("/home");
      }
    } catch {
      dispatch(error("Incorrect username or password"));
    }
  };

  return (
    <>
      <div className="container" style={{ maxWidth: 600 }}>
        <div className="row">
          <div className="col-12 text-center">
            <div className="shadow p-4 mt-4 bg-primary bg-gradient text-center rounded">
              <Link to="/ski-colorado" className="link-warning">
                <button className="butt btn-sm btn-primary">Back</button>
              </Link>
              <h1 className="snow">Login</h1>
              <h3>
                Don't have an account?&nbsp;&nbsp;
                <Link to="/signup" className="link-warning">
                  Click here!
                </Link>
              </h3>
              <br />
              <form onSubmit={handleSubmit} id="login-form">
                {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                <center>
                  <div className="w-75 mb-3">
                    <input
                      value={fields.username}
                      placeholder="Username"
                      type={`text`}
                      id="username"
                      className="form-control bg-secondary bg-opacity-50 bg-gradient"
                      onChange={(e) =>
                        dispatch(handleUsernameChange(e.target.value))
                      }
                    />
                  </div>
                </center>
                <center>
                  <div className="w-75 mb-3">
                    <input
                      value={fields.password}
                      placeholder="Password"
                      type={`password`}
                      id="password"
                      className="form-control bg-secondary bg-opacity-50 bg-gradient"
                      onChange={(e) =>
                        dispatch(handlePasswordChange(e.target.value))
                      }
                    />
                  </div>
                  <br />
                </center>
                <div className="button-box col d-flex justify-content-center">
                  <button className="butt btn-lg btn-primary">Log in</button>
                </div>
              </form>
              <div></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
